#define _DEFAULT_SOURCE

#include "test.h"

#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"


void debug (const char *fmt, ...);

static void *test_heap_init ();

static struct block_header *get_block_header (void *data);


static const size_t INITIAL_HEAP_SIZE = 10000;


void test1 (struct block_header *first_block)
{
    debug("Тест 1:  Однократное выделение и очистка\n");
    void *data = _malloc(3000);
    if (data == NULL)
    {
        err("Error: malloc вернул NULL");
    }
    debug_heap(stdout, first_block);
    if (first_block->is_free != false || first_block->capacity.bytes != 3000)
    {
        err("Error: неверный размер блока");
    }
    debug("Тест 1 пройден \n\n");
    _free(data);
}

void test2 (struct block_header *first_block)
{
    debug("Тест 2:  2 выделения - 1 очистка\n");
    void *data1 = _malloc(1000);
    void *data2 = _malloc(2000);
    if (data1 == NULL || data2 == NULL)
    {
        err("Error: malloc вернул NULL");
    }
    _free(data1);
    debug_heap(stdout, first_block);
    struct block_header *data1_block = get_block_header(data1);
    struct block_header *data2_block = get_block_header(data2);
    if (data1_block->is_free == false)
    {
        err("Error: 1 блок не очищен");
    }
    if (data2_block->is_free == true)
    {
        err("Error: 2 блок очищен");
    }
    debug("Tест 2 пройден\n\n");
    _free(data2);
}

void test3 (struct block_header *first_block)
{
    debug("Тест 3:  (удаление 2 блоков  из 3, центральный первым)\n");
    void *data1 = _malloc(2000);
    void *data2 = _malloc(1000);
    void *data3 = _malloc(1000);
    if (data1 == NULL || data2 == NULL || data3 == NULL)
    {
        err("Error: шибка: одно из выделений вернуло NULL");
    }
    _free(data2);
    _free(data1);
    debug_heap(stdout, first_block);
    struct block_header *data1_header = get_block_header(data1);
    struct block_header *data2_header = get_block_header(data2);
    struct block_header *data3_header = get_block_header(data3);
    if ((data1_header->is_free == false) || (data2_header->is_free == false))
    {
        err("Error: Очищенный блок 1 на самом деле не очищен(");
    }
    if (data3_header->is_free == true)
    {
        err("Error: Неочищенный блок очищен(");
    }
    if (data1_header->capacity.bytes != 3000 + offsetof(struct block_header, contents))
    {
        err("Error: Неверный размер блока");
    }
    debug("Tест 3 пройден\n\n");
    _free(data3);
}

void test4 (struct block_header *first_block)
{
    debug("Тест 4 -нам нужен больший регион\n");
    void *data1 = _malloc(10000);
    void *data2 = _malloc(10000);
    void *data3 = _malloc(10000);
    if (data1 == NULL || data2 == NULL || data3 == NULL)
    {
        err("Error: один из блоков - NULL");
    }
    debug_heap(stdout, first_block);
    struct block_header *data1_header = get_block_header(data1);
    struct block_header *data2_header = get_block_header(data2);
    if ((uint8_t *) data1_header->contents + data1_header->capacity.bytes != (uint8_t *) data2_header)
    {
        err("Error: Блок создан в ненужном месте");
    }
    debug("Tест 4 пройден\n\n");
    _free(data1);
    _free(data2);
    _free(data3);
}

void test5 (struct block_header *first_block)
{
    debug("Тест 5: новый регион выделяется в другом месте\n");

    void *data1 = _malloc(10000);
    if (data1 == NULL)
    {
        err("Error: Блок равен NULL");
    }
    struct block_header *start = first_block;
    while (start->next != NULL) start = start->next;
    map_pages(start, 1000, MAP_FIXED_NOREPLACE);
    void *data2 = _malloc(300000);
    debug_heap(stdout, first_block);
    struct block_header *data2_block = get_block_header(data2);
    if (data2_block == start)
    {
        err("Error: Блок выделился рядом");
    }
    debug("Tест 5 пройден\n\n");
    _free(data1);
    _free(data2);
}


void test_all ()
{
    struct block_header *first_block = (struct block_header *) test_heap_init();
    test1(first_block);
    test2(first_block);
    test3(first_block);
    test4(first_block);
    test5(first_block);
}


static void *test_heap_init ()
{
    debug("Выделение под тесты\n");
    void *heap = heap_init(INITIAL_HEAP_SIZE);
    if (heap == NULL)
    {
        err("Error: выделение не удалось");
    }
    return heap;
}

static struct block_header *get_block_header (void *data)
{
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}
